# sofie 

A powerful Matrix SelfBot. Powered by [mxbt](https://codeberg.org/librehub/mxbt).

## Installation

```sh
$ git clone https://codeberg.org/librehub/sofie
$ cd sofie
$ poetry run sofie
```

## Getting started

You need to create credits file:

**credits.json** structure
```json
{
    "homeserver" : "https://matrix.org",
    "user_id" : "user",
    "password" : "password"
}
```

## Contacts

| Contact                                               | Description       |
| :---:                                                 | :---              |
| [`Matrix`](https://matrix.to/#/#librehub:matrix.org)  | Matrix server     |

## Donates
**Monero/XMR:** `47KkgEb3agJJjSpeW1LpVi1M8fsCfREhnBCb1yib5KQgCxwb6j47XBQAamueByrLUceRinJqveZ82UCbrGqrsY9oNuZ97xN`

