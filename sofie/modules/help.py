from mxbt.module import Module, command
from mxbt import Context

class Help(Module):

    @command(aliases=['help'])
    async def help(self, ctx: Context) -> None:
        await ctx.edit("Help from [sofie](https://codeberg.org/librehub/sofie) :)", True)


