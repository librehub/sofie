from mxbt.module import Module, command
from mxbt import Context

import requests

class Translate(Module):

    async def __request(self, source: str, target: str, query: str) -> str:
        params = {
            'q' : query,
            'source' : source,
            'target' : target,
            'api_key' : ''
        }
        response = requests.post('https://libretranslate.org/translate', json=params)
        return response.json()['translatedText']

    @command(aliases=['tl'])
    async def translate(self, ctx: Context) -> None:
        try:
            source = ctx.args[0]
            target = ctx.args[1]
            query = ' '.join(ctx.args[2:])
            translated = await self.__request(source, target, query)
            await ctx.edit(
                translated + f'\n\n[Translated by LibreTranslate](https://libretranslate.org)',
                True
            )
        except Exception as e:
            await ctx.edit(f"Can't translate this message: \n\n{ctx.body}\n\nError: {e}")


