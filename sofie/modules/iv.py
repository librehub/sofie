from mxbt.module import Module, command
from invidious import Invidious
from mxbt import Context

class InvidiousModule(Module):

    iv = Invidious()

    @command(aliases=['iv.search'])
    async def search(self, ctx: Context) -> None:
        query = ' '.join(ctx.args)
        searched = self.iv.search(query, ctype='video')
        
        body = f"Result of search for: {query}\n\n"
        for video in searched:
            body += f"[{video.title}](https://yewtu.be/watch?v={video.videoId})\n\n"

        await ctx.edit(body, True)

    @command(aliases=['iv.info'])
    async def info(self, ctx: Context) -> None:
        url = ' '.join(ctx.args)
        index = url.find('?v=')
        video_id = url[index+3:]
        video = self.iv.get_video(video_id)
        if video is None:
            return await ctx.edit(f'Video with url: {url} not found!')

        body = (f"[{video.title}](https://yewtu.be/watch?v={video.videoId})\n"
                f"{video.descriptionHtml}\n"
                f"**Length:** {video.lengthSeconds}\n"
                f"**Rating:** {video.likeCount}/{video.dislikeCount}")

        await ctx.edit(body, True) 


