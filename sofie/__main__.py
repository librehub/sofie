
from mxbt import Bot, Creds
import json
import os

config = json.load(open('config.json', 'r'))

bot = Bot(
    creds=Creds.from_json_file('credits.json'),
    prefix=config['prefix'],
    selfbot=True,
    config=config
)

current_dir = os.path.dirname(__file__) + "/"
for filename in os.listdir(current_dir + "modules"):
    if filename.endswith(".py"):
        bot.mount_module(f"sofie.modules.{filename[:-3]}")

def main():
    bot.run()

if __name__ == "__main__":
    main()


